// Initialize Google client
(function(){
  gapi.load('auth2', function(){
    auth2 = gapi.auth2.init({
      client_id: '972690521745-qm2o2oqntlrqbdd42ctfem13is3ua48q.apps.googleusercontent.com',
      cookiepolicy: 'single_host_origin',
    })
  })
}())

function googleSignIn(callback) {
  auth2.signIn().then(() => {
    callback(auth2.currentUser.get())
  })
}

function googleSignOut(callback) {
  var auth2 = gapi.auth2.getAuthInstance()
  auth2.signOut().then(callback)
}
